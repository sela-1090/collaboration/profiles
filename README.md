1. Each student creates (if not exists already) a new GitLab user.
2. Each student creates issue in this project.
3. Each student creates a feature branch (from within the issue) for this repository and add a folder named with the GitLab username (e.g. ez) with a README.md file (see example: ) that includes the following information:

    - The Avatar (linked to a PNG file named as the GitLab-username with .png extension)
    - GitLab user name
    - Student real name
    - email address
4. create a merge-request for the feature branch into the main branch
5. the project-managers merge the feature-branch into main and close the issue
